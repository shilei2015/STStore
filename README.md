# STStore

[![CI Status](https://img.shields.io/travis/shilei2015/STStore.svg?style=flat)](https://travis-ci.org/shilei2015/STStore)
[![Version](https://img.shields.io/cocoapods/v/STStore.svg?style=flat)](https://cocoapods.org/pods/STStore)
[![License](https://img.shields.io/cocoapods/l/STStore.svg?style=flat)](https://cocoapods.org/pods/STStore)
[![Platform](https://img.shields.io/cocoapods/p/STStore.svg?style=flat)](https://cocoapods.org/pods/STStore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

STStore is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'STStore'
```

## Author

shilei2015, 244235126@qq.com

## License

STStore is available under the MIT license. See the LICENSE file for more info.
