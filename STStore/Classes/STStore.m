//
//  STStore.m
//  STStore
//
//  Created by EDY on 2023/7/17.
//

#import "STStore.h"
#import <RMStore/RMStore.h>

@interface STStore ()<RMStoreReceiptVerificator,STStoreDelegate>

@property (nonatomic , strong) NSMutableDictionary * tempDict;

@end


@implementation STStore

+ (instancetype)sharedInstance {
    
    static STStore * _sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [STStore new];
    });
    
    return _sharedManager;
}

static Class<STStoreDelegate> _storeClass = nil;
+ (void)setStoreDelegateClass:(Class)cls {
    _storeClass = cls;
}


#pragma mark 基本功能
+ (void)addPayment:(NSString *)paymentId productId:(NSString *)productId success:(nullable STStoreHandle)success fail:(nullable STStoreHandleFail)fail
{
    if (_storeClass) {
        
        [_storeClass prePayWithId:paymentId productId:productId ok:^(id  _Nonnull data) {
            
            if (data) {[[STStore sharedInstance].tempDict setObject:data forKey:paymentId];}
            
            [self Payment:paymentId success:success fail:fail];

        } fail:fail];

    } else {
        
        [self Payment:paymentId success:success fail:fail];
        
    }
}


+ (void)checkMissingPayments:(NSArray <NSString *>*)payments {
    [RMStore defaultStore].receiptVerificator = [self sharedInstance];
    
    NSSet * set = [NSSet setWithArray:payments];
    [[RMStore defaultStore] requestProducts:set];
}

- (NSMutableDictionary *)tempDict {
    if (!_tempDict) {
        _tempDict = [NSMutableDictionary new];
    }
    return _tempDict;
}


#pragma mark RMStoreReceiptVerificator
- (void)verifyTransaction:(SKPaymentTransaction*)transaction
                  success:(void (^)(void))successBlock
                  failure:(void (^)(NSError *error))failureBlock {
    
    NSData* data = [NSData dataWithContentsOfURL:[NSBundle mainBundle].appStoreReceiptURL];
    NSString * reciptString = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSString * pid = transaction.payment.productIdentifier;
    
    id oid = [[STStore sharedInstance].tempDict objectForKey:pid];
    
    [_storeClass verifyTransaction:reciptString pid:pid oid:oid ok:^(id  _Nonnull data) {
        if (successBlock) {successBlock();}
    } fail:^(id  _Nonnull error) {
        if (failureBlock) {failureBlock(error);}
    }];
    
}



#pragma mark 内部方法
+ (void)Payment:(NSString *)paymentId success:(nullable STStoreHandle)success fail:(nullable STStoreHandleFail)fail
{
    NSSet * set = [NSSet setWithArray:@[paymentId]];
    
    [[RMStore defaultStore] requestProducts:set success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        
        if (products.count) {
            
            [[RMStore defaultStore] addPayment:paymentId success:^(SKPaymentTransaction *transaction) {
                [[STStore sharedInstance].tempDict removeObjectForKey:paymentId];
                if (success) {success();}
            } failure:^(SKPaymentTransaction *transaction, NSError *error) {
                if (fail) {fail(error);}
            }];
            
        } else {
            
            NSString * kErrorDomain = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
            NSError *err = [NSError errorWithDomain:kErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Invalid item"}];
            
            if (fail) {fail(err);}
        }
        
    } failure:^(NSError *error) {
        if (fail) {fail(error);}
    }];
}



+ (NSString *)errorTip:(NSError *)error {
    
    NSString * message = nil;
    
    switch (error.code) {
        case SKErrorUnknown:
            message = @"Unkown error";
            break;
            
        case SKErrorClientInvalid:
            message = @"Client is not allowed to issue the request, etc";
            break;
            
        case SKErrorPaymentCancelled://取消
            message = @"User cancelled the request, etc";
            break;
            
        case SKErrorPaymentInvalid:
            message = @"Purchase identifier was invalid, etc";
            break;
            
        case SKErrorPaymentNotAllowed://设备不支持支付
            message = @"This device is not allowed to make the payment";
            break;
            
        case SKErrorStoreProductNotAvailable:
            message = @"Product is not available in the current storefront";
            break;
            
        case SKErrorCloudServicePermissionDenied:
            message = @"User has not allowed access to cloud service information";
            break;
            
        case SKErrorCloudServiceNetworkConnectionFailed:
            message = @"The device could not connect to the nework";
            break;
            
        case SKErrorCloudServiceRevoked:
            message = @"User has revoked permission to use this cloud service";
            break;
            
        case SKErrorPrivacyAcknowledgementRequired:
            message = @"The user needs to acknowledge Apple's privacy policy";
            break;
            
        case SKErrorUnauthorizedRequestData:
            message = @"The app is attempting to use SKPayment's requestData property, but does not have the appropriate entitlement";
            break;
            
        case SKErrorInvalidOfferIdentifier:
            message = @"The specified subscription offer identifier is not valid";
            break;
            
        case SKErrorInvalidSignature:
            message = @"The cryptographic signature provided is not valid";
            break;
            
        case SKErrorMissingOfferParams:
            message = @"One or more parameters from SKPaymentDiscount is missing";
            break;
            
        case SKErrorInvalidOfferPrice:
            message = @"The price of the selected offer is not valid (e.g. lower than the current base subscription price)";
            break;
            
        case SKErrorOverlayCancelled:
            message = @"OverlayCancelled";
            break;
            
        case SKErrorOverlayInvalidConfiguration:
            message = @"OverlayInvalidConfiguration";
            break;
            
        case SKErrorOverlayTimeout:
            message = @"OverlayTimeout";
            break;
            
        case SKErrorIneligibleForOffer:
            message = @"User is not eligible for the subscription offer";
            break;
            
        case SKErrorUnsupportedPlatform:
            //平台不支持支付
            message = @"UnsupportedPlatform";
            
        default:
            break;
    }
    
    return message;
    
    
    
}


@end
