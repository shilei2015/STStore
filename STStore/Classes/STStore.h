//
//  STStore.h
//  STStore
//
//  Created by EDY on 2023/7/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^STStoreHandleOK)(id data);
typedef void(^STStoreHandleFail)(id error);
typedef void(^STStoreHandle)(void);

@protocol STStoreDelegate <NSObject>

///预订单处理
+ (void)prePayWithId:(NSString *)paymentId productId:(NSString *)productId ok:(nullable STStoreHandleOK)ok fail:(nullable STStoreHandleFail)fail;

+ (void)verifyTransaction:(NSString *)reciptStr pid:(NSString *)pid oid:(NSString *)oid ok:(nullable STStoreHandleOK)ok fail:(nullable STStoreHandleFail)fail;

@end



@interface STStore : NSObject


+ (void)setStoreDelegateClass:(Class)cls;

+ (void)checkMissingPayments:(NSArray <NSString *>*)payments;

+ (void)addPayment:(NSString *)paymentId productId:(NSString *)productId success:(nullable STStoreHandle)success fail:(nullable STStoreHandleFail)fail;

+ (NSString *)errorTip:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
